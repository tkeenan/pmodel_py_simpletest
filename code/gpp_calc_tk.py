# -*- coding: utf-8 -*-
"""
Created on Oct 20th 2017

@author: T. Keenan
"""

# this script will...
# run the p-model implementation based on Wang Han's 2017 Nature Plants version
# for set values of PPFD, Air T, VPD, CO2, FAPAR, Alpha

######## Import modules#########
import os
import sys

myhome = os.path.expanduser("~")

import numpy as np
import matplotlib.pyplot as plt

##############
# Main program
##############

         
# Testing out GPP claucation with pseudo data:
    
ppfd = np.float64(2000.0) #
this_fapar = np.float64(1.0) #average
temp = np.float64(20.0) # deg C
alpha = np.float64(1.0) # No water limitation
elv = np.float64(0.0) # Sea level
beta = np.float64(146.0) # Standard beta 146.0
vpd = np.float64(1000.0) #Pa
aCO2 = np.float64(400.0) #Ppm

k_temp = temp + kelv2cel  #temp in kelvin
this_vpd = vpd
patm = 101.325 * 1000 	# potentially different from Rebecca version

pp_CO2 = aCO2 * 1E-6 * patm # CO2 in Pa--> partial pressure for average atmospheroc pressure 
							# CO2 ppm *  1/10^-6 * 

# calculate K1            
P0_out = kco * (1e-6) * patm
tempFracK0=((k_temp - t_25) * Ha_ko) / (R * k_temp * t_25)
K0_out=ko25 * np.exp(tempFracK0)
tempFrac=((k_temp - t_25) * Ha_kc) / (R * k_temp * t_25)
Kc_out = kc25 * np.exp(tempFrac)

k1_out = Kc_out * (1 + ( P0_out / K0_out))

# calculate gamma* and eta*
ttg=((k_temp - t_25) * Ha) / (R * k_temp * t_25)
gs=gamma_25 * np.exp(ttg)
eta_star= np.exp(eta_const * (k_temp - t_25)); 
# this is the viscocity of water at current T relative to at T25

m_calc = (pp_CO2 - gs) / \
			(pp_CO2 + (2 * gs) + \
			(3 * gs) * \
		    np.sqrt(1.6 * eta_star * \
		    this_vpd * \
		    (beta ** -1.0) * \
		    ((k1_out + gs) ** -1.0)))
		    
m_frac = np.sqrt(1.0 - ((c_star / m_calc) ** ( 2.0 / 3.0 )))

# GPP is zero for monhtly mean temepratures below 0C
lue = phi_0 *\
		m_calc * \
		m_frac *\
		(alpha ** (1.0/4.0))
		
gpp = lue * \
		absG * \
		ppfd * \
		this_fapar 
